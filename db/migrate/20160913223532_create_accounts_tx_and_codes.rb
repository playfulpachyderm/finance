class CreateAccountsTxAndCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :accounts do |t|
      t.string :name,                 :null => false, :index => true
      t.integer :code,                :null => false, :index => true
      t.timestamps
    end
    create_table :txes do |t|
      t.references :dr,               :null => false, :index => true
      t.references :cr,               :null => false, :index => true
      t.date :date,                   :null => false, :index => true
      t.decimal :amount, :precision => 8, :scale => 2, :null => false
      t.text :description
      t.timestamps
    end
    create_table :account_codes do |t|
      t.string :pattern,              :null => false, :index => true
      t.string :name,                 :null => false, :index => true
      t.timestamps
    end
  end
end
