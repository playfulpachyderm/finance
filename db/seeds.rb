# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


# /**********************************************************************
# ************************ CHART OF ACCOUNTS ****************************
# *
# * 	Use this template when creating accounts.
# *
# **********************************************************************/

AccountCode.create(:pattern => '1__', :name => 'ASSETS')
AccountCode.create(:pattern => '10_', :name => 'CURRENT ASSETS')
AccountCode.create(:pattern => '11_', :name => 'LONG TERM ASSETS')

AccountCode.create(:pattern => '2__', :name => 'LIABILITIES')
AccountCode.create(:pattern => '20_', :name => 'CURRENT LIABILITIES')
AccountCode.create(:pattern => '21_', :name => 'LONG TERM LIABILITIES')

AccountCode.create(:pattern => '4__', :name => 'REVENUE')

AccountCode.create(:pattern => '5__', :name => 'LEGITIMATE EXPENSES')
AccountCode.create(:pattern => '6__', :name => 'OPTIONAL EXPENSES')
AccountCode.create(:pattern => '7__', :name => 'RETARDED EXPENSES')
AccountCode.create(:pattern => '8__', :name => 'MISC EXPENSES')

# ==== Accounts ====

Account.create(:code => 100, :name => 'CIBC Chequing')
Account.create(:code => 101, :name => 'CIBC US-dollar')
Account.create(:code => 102, :name => 'US Bank Chequing')
Account.create(:code => 103, :name => 'PC Financial Chequing')
Account.create(:code => 110, :name => 'CIBC Visa security deposit')
Account.create(:code => 111, :name => 'US Bank Savings')
Account.create(:code => 112, :name => 'MD Management')

Account.create(:code => 200, :name => 'CIBC Visa')
Account.create(:code => 210, :name => 'Debts to Jefferey Lang')

Account.create(:code => 400, :name => 'Salary')
Account.create(:code => 401, :name => 'Bonus')
Account.create(:code => 402, :name => 'Interest')

Account.create(:code => 500, :name => 'Rent')
Account.create(:code => 501, :name => 'Groceries')
Account.create(:code => 502, :name => 'Textbooks')
Account.create(:code => 503, :name => 'Car maintainence')
Account.create(:code => 504, :name => 'Supplies')
Account.create(:code => 505, :name => 'Haircuts')
Account.create(:code => 506, :name => 'Clothes')
Account.create(:code => 507, :name => 'Transportation and Travel')
Account.create(:code => 508, :name => 'Gym / Fitness')
Account.create(:code => 509, :name => 'Home equipment')

Account.create(:code => 600, :name => 'Alcohol')
Account.create(:code => 601, :name => 'Books')
Account.create(:code => 602, :name => 'Games')
Account.create(:code => 603, :name => 'Cologne')
Account.create(:code => 604, :name => 'Entertainment')
Account.create(:code => 605, :name => 'Stuff')

Account.create(:code => 610, :name => 'Shawarma')
Account.create(:code => 611, :name => 'Pizza')
Account.create(:code => 612, :name => 'Sushi')
Account.create(:code => 613, :name => 'Other fast food')
Account.create(:code => 650, :name => 'Fine dining')

Account.create(:code => 700, :name => 'Fees')
Account.create(:code => 701, :name => 'Parking')
Account.create(:code => 702, :name => 'Unknown')

Account.create(:code => 800, :name => 'Tuition')
# Account.create(:code => 800, :name => 'Quest: Co-op Fee')
# Account.create(:code => 801, :name => 'Quest: ENG Endowment Fund')
# Account.create(:code => 802, :name => 'Quest: ENG Student Society')
# Account.create(:code => 803, :name => 'Quest: Federation of Students Fee')
# Account.create(:code => 804, :name => 'Quest: FedofStdnts-Administered Fees')
# Account.create(:code => 805, :name => 'Quest: Health Services Building Fee')
# Account.create(:code => 806, :name => 'Quest: Imprint')
# Account.create(:code => 807, :name => 'Quest: Sandford Fleming Foundation')
# Account.create(:code => 808, :name => 'Quest: Student Services Fee')
# Account.create(:code => 809, :name => 'Quest: Undergrad Full time Tuition')
# Account.create(:code => 810, :name => 'Quest: WPIRG')
# Account.create(:code => 811, :name => 'Quest: Work Report Marking Fee')
# Account.create(:code => 812, :name => 'Quest: Late Fee')
# Account.create(:code => 813, :name => 'Quest: University of Waterloo President\'s Scholarship')
# Account.create(:code => 814, :name => 'Quest: ST Jeromes Residence Fee')
# Account.create(:code => 815, :name => 'Quest: Orientation Fee')

# ==== TX ====

# Account.all.each do |acct|
#   regex = acct.regex
#   next if !regex

#   csv = File.read("lib/#{acct.code}.csv").split("\n")
#   csv.each do |line|
#   end
# end
# # CIBC chequing


# csv = File.read("lib/cibc_chequing.csv").split("\n")
# line_regex = /(?<date>\d\d\d\d-\d\d-\d\d),(?<description>[^,]*)\s*,(?<dr>[0-9.]*),(?<cr>[0-9.]*)/
# csv.each do |line|
#   line
# end

if Rails.env.development?

end
