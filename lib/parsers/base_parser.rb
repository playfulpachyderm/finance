class BaseParser
  attr_accessor :unknowns
  def initialize(acct, manual)
    @acct = acct
    @manual = manual
    @unknowns = []
  end

  def acct_type
    return
  end

  def tx_type(dr, cr)
    # 1 -> acct gets debited
    # -1 -> acct gets credited
    if dr.to_f != 0
      return 1
    else
      return -1
    end
  end

  def to_amount(dr, cr)
    case tx_type(dr, cr)
    when 1
      return dr.to_f
    when -1
      return cr.to_f
    else
      raise "Unknown type: " + tx_type(dr, cr).to_s
    end
  end

  def accounts_from_amounts(dr, cr, guess)
    case tx_type(dr, cr) * @acct.direction
    when 1
      return @acct.code, guess
    when -1
      return guess, @acct.code
    end
  end
end
