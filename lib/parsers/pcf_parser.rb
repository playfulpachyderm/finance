class PcfParser < BaseParser
  def parse_file(s)
    s.split("\n").drop(1).each { |line| parse_line(line) }
  end

  def parse_line(s)
    guess = guesser(s)
    if (guess.nil? || guess < 0) && @manual
      puts "unknown: #{s}"
      guess = STDIN.gets.to_i
    end
    if guess.nil? || guess < 100
      @unknowns << s
      return nil  # ignore this line (don't create a tx)
    end

    date, description, cr, dr = s.split(",")
    m,d,y = date.split("/")
    date = [y,m,d].join("-")

    amount = to_amount(dr, cr)

    dr, cr = accounts_from_amounts(dr, cr, guess)
    puts "#{amount}, #{dr}, #{cr}"

    tx = Tx.new(
      :cr => Account.find_by(:code => cr),
      :dr => Account.find_by(:code => dr),
      :amount => amount,
      :description => description,
      :date => date
    )

    if !tx.save
      @unknowns << "FAILURE!!!!" + s
    end
  end

  def guesser(s)
    return -1  if s.match(/transfer in/i)             # ignore
    return -1  if s.match(/transfer out/i)            # ignore
    return -1  if s.match(/bill pay.*mastercard/i)    # ignore

    return 402 if s.match(/interest/i)                # interest
    return 700 if s.match(/e-transfer send s\/c/i)    # fees
    return 700 if s.match(/abm interac charge/i)      # fees
    return 500 if s.match(/leyla demir/i)             # rent
    return 500 if s.match(/arulrasa/i)                # rent
    return 500 if s.match(/zielinska/i)               # rent
    return 500 if s.match(/ankit gupta/i)             # rent
    return 800 if s.match(/bill pay.*university/i)    # tuition
    return 400 if s.match(/payroll/i)                 # salary
    return 613 if s.match(/rotisserie roma/i)         # other fast food
    return 613 if s.match(/la banquis/i)              # other fast food
    return 613 if s.match(/les belles/i)              # other fast food
    return 613 if s.match(/hanze el/i)                # other fast food
    return 613 if s.match(/subway/i)                  # other fast food
    return 505 if s.match(/first choice/i)            # haircuts
    return 610 if s.match(/al.?madina/i)              # shawarma
    return 610 if s.match(/pita/i)                    # shawarma
    return 501 if s.match(/zehrs/i)                   # groceries
    return 507 if s.match(/eft.*vision critical/i)    # travel and transportation
    return 507 if s.match(/vancouver taxi/i)          # travel and transportation
    return 604 if s.match(/calgary climb/i)           # entertainment
    return 210 if s.match(/e-transfer.*jefferey/i)    # entertainment

    return nil if s.match(/cheque/i)                  # unknown
    return nil if s.match(/abm interac/i)             # unknown
    return nil if s.match(/abm deposit/i)             # unknown
  end
end
