class CibcParser < BaseParser
  def parse_file(s)
    s.split("\n").each { |line| parse_line(line) }
  end

  def parse_line(s)
    guess = guesser(s)
    if (guess.nil? || guess < 0) && @manual
      puts "unknown: #{s}"
      guess = STDIN.gets.to_i
    end
    if guess.nil? || guess < 100
      @unknowns << s
      return nil  # ignore this line (don't create a tx)
    end

    date, description, cr, dr = s.split(",")
    amount = to_amount(dr, cr)

    dr, cr = accounts_from_amounts(dr, cr, guess)
    puts "#{amount}, #{dr}, #{cr}"

    tx = Tx.new(
      :cr => Account.find_by(:code => cr),
      :dr => Account.find_by(:code => dr),
      :amount => amount,
      :description => description,
      :date => date
    )

    if !tx.save
      @unknowns << "FAILURE!!!!" + s
    end
  end

  def guesser(s)
    return -1  if s.match(/toms refuse/i)             # ignore
    return -1  if s.match(/internet banking.*/i)      # ignore
    return -1  if s.match(/rebate/i)                  # ignore
    return -1  if s.match(/preauthorized/i)           # ignore
    return -1  if s.match(/cibcsi/i)                  # ignore (CIBC Securities Inc., i.e. mutual fund)
    return -1  if s.match(/SLEEP COUNTRY.*30.00/i)    # ignore

    return 604 if s.match(/netflix/i)                 # entertainment
    return 700 if s.match(/service charge/i)          # fees
    return 700 if s.match(/fee/i)                     # fees
    return 100 if s.match(/thank you/i)               # CIBC chequing
    return 501 if s.match(/sobeys/i)                  # groceries
    return 501 if s.match(/zehrs/i)                   # groceries
    return 501 if s.match(/shopper'?s drug mart/i)    # groceries
    return 501 if s.match(/safeway/i)                 # groceries
    return 501 if s.match(/food basics/i)             # groceries
    return 501 if s.match(/wal.?mart/i)               # groceries
    return 501 if s.match(/family foods/i)            # groceries
    return 501 if s.match(/popeyes/i)                 # groceries
    return 501 if s.match(/e-transfer.*parsa/i)       # groceries
    return 501 if s.match(/clancey/i)                 # groceries
    return 508 if s.match(/fitness/i)                 # gym / fitness
    return 400 if s.match(/dofasco/i)                 # salary
    return 400 if s.match(/payroll/i)                 # salary
    return 400 if s.match(/willet/i)                  # salary
    return 610 if s.match(/mozy/i)                    # shawarma
    return 610 if s.match(/phat-hat/i)                # shawarma
    return 610 if s.match(/montfort/i)                # shawarma
    return 610 if s.match(/doner kabab/i)             # shawarma
    return 610 if s.match(/al.?madina/i)              # shawarma
    return 610 if s.match(/shawarma/i)                # shawarma
    return 610 if s.match(/pita/i)                    # shawarma
    return 800 if s.match(/bill pay.*university/i)    # tuition
    return 509 if s.match(/target/i)                  # home equipment
    return 509 if s.match(/cdn tire/i)                # home equipment
    return 600 if s.match(/lcbo/i)                    # alcohol
    return 600 if s.match(/avondale/i)                # alcohol
    return 600 if s.match(/wine shop/i)               # alcohol
    return 612 if s.match(/sushi/i)                   # sushi
    return 612 if s.match(/adami/i)                   # sushi
    return 612 if s.match(/wind st catharines/i)      # sushi
    return 612 if s.match(/publix/i)                  # sushi
    return 612 if s.match(/august 8/i)                # sushi
    return 612 if s.match(/pizza/i)                   # pizza
    return 612 if s.match(/twice the deal/i)          # pizza
    return 601 if s.match(/books/i)                   # books
    return 601 if s.match(/indigo/i)                  # books
    return 601 if s.match(/chapters/i)                # books
    return 601 if s.match(/words worth/i)             # books
    return 504 if s.match(/staples/i)                 # supplies
    return 504 if s.match(/univ of w retail/i)        # supplies
    return 507 if s.match(/united airline/i)          # travel and transportation
    return 507 if s.match(/swell bicycles/i)          # travel and transportation
    return 507 if s.match(/air canada/i)              # travel and transportation
    return 507 if s.match(/sfmta/i)                   # travel and transportation
    return 500 if s.match(/brett turner/i)            # rent
    return 500 if s.match(/cpc\/scp/i)                # rent (canada post)
    return 506 if s.match(/macy/i)                    # clothes
    return 602 if s.match(/steam/i)                   # games
    return 402 if s.match(/interest/i)                # interest
    return 503 if s.match(/jc framar/i)               # car maintainance
    return 503 if s.match(/esso/i)                    # car maintainance
    return 701 if s.match(/park/i)                    # parking
    return 613 if s.match(/subway/i)                  # other fast food
    return 613 if s.match(/eat24/i)                   # other fast food
    return 613 if s.match(/burrito boyz/i)            # other fast food
    return 613 if s.match(/jimmy the greek/i)         # other fast food
    return 613 if s.match(/saigon/i)                  # other fast food
    return 613 if s.match(/east side mario/i)         # other fast food
    return 613 if s.match(/mikey's eatery/i)          # other fast food
    return 613 if s.match(/zoup/i)                    # other fast food
    return 613 if s.match(/mccabe's/i)                # other fast food
    return 613 if s.match(/mandarin/i)                # other fast food
    return 613 if s.match(/busan korean bbq/i)        # other fast food
    return 613 if s.match(/burger/i)                  # other fast food
    return 613 if s.match(/sing lee/i)                # other fast food
    return 613 if s.match(/skylon food service/i)     # other fast food
    return 613 if s.match(/queen.*head/i)             # other fast food
    return 613 if s.match(/casablanca/i)              # other fast food
    return 505 if s.match(/haircut/i)                 # haircuts
    return 505 if s.match(/first choice/i)            # haircuts
    return 505 if s.match(/great clips/i)             # haircuts
    return 603 if s.match(/the bay/i)                 # cologne
    return 603 if s.match(/the body shop/i)           # cologne
    return 604 if s.match(/sympbxoffice/i)            # entertainment
    return 604 if s.match(/cineplex/i)                # entertainment
    return 604 if s.match(/new world/i)               # entertainment
    return 501 if s.match(/instant teller.*22.99/i)   # groceries (actually laundry but meh)
    return 500 if s.match(/branch trans.*3224.58/i)   # rent (paid by money draft to Cheryl)
    return 112 if s.match(/instant teller.*1500.43/i) # MDM
    return 210 if s.match(/instant teller.*9500.00/i) # debts to Jefferey Lang

    return nil if s.match(/instant teller/i)          # unknown
    return nil if s.match(/branch transaction/i)      # unknown
  end
end

  #   if matches("pre.?authorized", "thank you", "internet transfer", "rebate"):
  #     return -1

  #   if matches("instant teller"):
  #     return None  # user decides

  #   if matches("dofasco", "payroll", "zenefits", "willet"):
  #     return 400
  #   if matches("interest", "dividend"):
  #     return 402
  #   if matches("brett turner", "cpc"):
  #     return 500
  #   if matches("bill pay.*university"):
  #     return -1  # covered in permanent_txs
  #   if matches("zehrs", "sobeys", "food", "safeway", "popeyes", "clancey", "transfer.*parsa alipour",
  #          "wal.?mart", "drug mart", "walgreens", "7.*eleven", "soylent", "liquor groce"):
  #     return 501
  #   if matches("univ.*retail"):
  #     return 502
  #   if matches("jc framar"):
  #     return 503
  #   if matches("staples"):
  #     return 504
  #   if matches("great clips", "first choice", "haircut"):
  #     return 505
  #   if matches("macy.*51.65 USD", "macy.*78"):
  #     return 506
  #   if matches("united", "sfmta", "swell bicycles", "air canada", "esso", "clipper",
  #          "lyft"):
  #     return 507
  #   if matches("24 ?hour fitness"):
  #     return 508
  #   if matches("lcbo", "wine", "avondale"):
  #     return 600
  #   if matches("books", "chapters", "indigo", "words worth"):
  #     return 601
  #   if matches("steam"):
  #     return 602
  #   if matches("deb merch return macy", "macy.*88.09 USD", "body shop", "bay"):
  #     return 603
  #   if matches("sanfran.*symp", "cine"):
  #     return 604
  #   if matches("target", "new world recor", "cdn tire"):
  #     return 605
  #   if matches("shawarma", "mozy", "montfort", "madina", "pita", "phat.*hat", "doner"):
  #     return 610
  #   if matches("pizza", "twice", "eat24"):
  #     return 611
  #   if matches("sushi", "wind", "august 8", "adami", "publix"):
  #     return 612
  #   if matches("burger", "subway", "jimmy.*greek", "mandarin", "mikey", "burrito", "mccabe",
  #          "zoup", "east side", "queen.*head", "sing lee", "chipotle", "casablanca"):
  #     return 613
  #   if matches("crab", "saigon", "star india"):
  #     return 650
  #   if matches("fee", "service charge"):
  #     return 700
  #   if matches("park"):
  #     return 701

  #   return None
