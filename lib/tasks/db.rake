namespace :db do
  desc "TODO"
  task :populate => :environment do
  	puts "Populating txs.\n\n"
    parser_dir = "lib/parsers"
    Dir[parser_dir + "/*.rb"].each { |x|
      puts "requiring #{x}"
      require Rails.root.join(x)
    }

    csv_dir = "lib/acct_csvs"

    map = {"100" => CibcParser, "103" => PcfParser, "200" => CibcParser, "201" => PcmcParser}
    unknowns = {}
    Dir[csv_dir + "/*.csv"].each do |x|
      puts "Parsing #{x}"
      file = File.read(x)
      filename = File.basename(x).match(/\d{3}/).to_s
      parser = map[filename]

      if parser
      	puts "Parsing using parser: #{parser}"
      	acct = Account.find_by(:code => filename)
        p = parser.new(acct, false)
        p.parse_file(file)
        unknowns[x] = p.unknowns
      else
      	puts "No parser found!"
      end

    end
    File.open("unknowns.txt", "w") do |file|
      unknowns.each do |key, value|
        file.write(key + ":\n")
        value.each do |val|
          file.write(val + "\n")
        end
        file.write("\n")
      end
    end
    # csvs = Dir.entries(csv_dir).select { |x| File.file?("#{csv_dir}/#{x}") }
    # csvs.each do |csv|

    # end

  end
end
