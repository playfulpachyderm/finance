class Tx < ApplicationRecord
  belongs_to :dr, :class_name => 'Account'
  belongs_to :cr, :class_name => 'Account'

  validates :amount, :presence => true
  validates :date, :presence => true

  validates :dr, :presence => true
  validates :dr_id, :presence => true
  validates :cr, :presence => true
  validates :cr_id, :presence => true

  validate :amount_is_positive

  private

    def amount_is_positive
      if amount && amount <= 0
        errors.add(:amount, "must be a positive number")
      end
    end
end
