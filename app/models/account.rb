class Account < ApplicationRecord
  has_many :drs, :class_name => 'Tx', :foreign_key => 'dr_id'
  has_many :crs, :class_name => 'Tx', :foreign_key => 'cr_id'

  validates :name, :presence => true, :uniqueness => true
  validates :code, :presence => true, :uniqueness => true

  validate :code_matches_patterns

  def account_types
    AccountCode.where("? like pattern", code)
  end

  def balance(date = Time.now)
    # return the "change" since Jan 1, 0000.  Will fail for txes done in prehistory.
    time1 = Time.new(0, 1, 1)
    time2 = date
    return change(time1, time2)
  end

  def direction
    return AccountCode.account_direction(code)
  end

  def change(time1, time2)
    date1 = time1.strftime("%Y-%m-%d")
    date2 = time2.strftime("%Y-%m-%d")
    debits = drs.where('? <= date and date < ?', date1, date2)
    credits = crs.where('? <= date and date < ?', date1, date2)
    sum = debits.sum(:amount) - credits.sum(:amount)
    return 0 if sum == 0
    return sum * direction
  end

  private

    def code_matches_patterns
      if account_types.empty?
        errors.add(:code, "must match at least one account type")
      end
    end
end
