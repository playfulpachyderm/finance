class AccountCode < ApplicationRecord
  validates :name, :presence => true, :uniqueness => true
  validates :pattern, :presence => true, :uniqueness => true

  ASSETS = "ASSETS"
  LIABILITIES = "LIABILITIES"
  REVENUE = "REVENUE"
  EXPENSES = "EXPENSES"

  def regex
    return Regexp.new("^" + pattern.gsub("_", "\\d") + "$")
  end

  def check(account_code)
    return !!(regex.match(account_code.to_s))
  end

  def self.account_direction(account_code)
    [ASSETS, EXPENSES].each do |type|
      AccountCode.where("name like ?", "%#{type}").each do |acct_code|
        if acct_code.check(account_code)
          return 1
        end
      end
    end
    [LIABILITIES, REVENUE].each do |type|
      AccountCode.where("name like ?", "%#{type}").each do |acct_code|
        if acct_code.check(account_code)
          return -1
        end
      end
    end

    return nil
  end

  def accounts
    return Account.where("code like ?", pattern)
  end

  def total(date1 = Time.now, date2 = nil)
    if date2
      return accounts.sum { |acct| acct.change(date1, date2) }
    else
      return accounts.sum { |acct| acct.balance(date1) }
    end
  end
end
