class FinancialStatementsController < ApplicationController
  include FinancialStatementsHelper

  def balance_sheet
    @date = parse_date(params[:date], Time.now)

    @current_assets        = AccountCode.find_by(:name => "CURRENT ASSETS")
    @long_term_assets      = AccountCode.find_by(:name => "LONG TERM ASSETS")
    @current_liabilities   = AccountCode.find_by(:name => "CURRENT LIABILITIES")
    @long_term_liabilities = AccountCode.find_by(:name => "LONG TERM LIABILITIES")
  end

  def income_statement
    date1 = parse_date(params[:date1], Time.new(0, 1, 1))
    date2 = parse_date(params[:date2], Time.new(2100, 1, 1))

    @dates = [date1, date2]

    @revenue = AccountCode.find_by(:name => "REVENUE")
    @legit_expenses = AccountCode.find_by(:name => "LEGITIMATE EXPENSES")
    @optional_expenses = AccountCode.find_by(:name => "OPTIONAL EXPENSES")
    @retarded_expenses = AccountCode.find_by(:name => "RETARDED EXPENSES")
    @misc_expenses = AccountCode.find_by(:name => "MISC EXPENSES")
  end
end
