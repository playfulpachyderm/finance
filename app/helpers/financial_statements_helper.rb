module FinancialStatementsHelper
  def date_format_human(time)
    return time.strftime("%B %d, %Y")
  end

  def date_format_db(time)
    return time.strftime("%Y-%m-%d")
  end

  def parse_date(str, default = Time.now)
    if str
      return Time.strptime(str, "%Y-%m-%d")
    else
      return default
    end
  end

  def format_balance(balance)
    return balance == 0 ? "--" : "%.2f" % balance
  end
end
