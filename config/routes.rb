Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/balance_sheet',    :to => 'financial_statements#balance_sheet'
  get '/income_statement',    :to => 'financial_statements#income_statement'
end
