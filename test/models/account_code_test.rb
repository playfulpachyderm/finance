require 'test_helper'

class AccountCodeTest < ActiveSupport::TestCase

  def setup
    @acct_code = account_codes(:assets)
  end

  # ==== Validation ====

  test 'should be valid' do
    assert @acct_code.valid?
  end

  test 'should not accept duplicate patterns' do
    a = AccountCode.new(:pattern => @acct_code.pattern, :name => "fjawkefj")
    assert_not a.valid?
  end

  test 'should not accept duplicate names' do
    a = AccountCode.new(:pattern => /asdf/, :name => @acct_code.name)
    assert_not a.valid?
  end

  test 'should not accept blank names' do
    @acct_code.name = "     "
    assert_not @acct_code.valid?
  end

  test 'should not accept blank patterns' do
    @acct_code.pattern = nil
    assert_not @acct_code.valid?
  end

  # ==== Pattern tests ====

  test 'regex should be a regex' do
    assert_equal /^1\d\d$/, @acct_code.regex
  end

  test 'should check code matches properly' do
    assert @acct_code.check("100")
    assert @acct_code.check("158")
  end

  test 'should fail to match wrong code' do
    assert_not @acct_code.check("1000")
    assert_not @acct_code.check("1158")
    assert_not @acct_code.check("200")
    assert_not @acct_code.check("asd")
  end

  # ==== Account tests ====

  test 'should determine whether cr or dr account' do
    assert_equal 1, AccountCode.account_direction(accounts(:bank_acct).code)
    assert_equal 1, AccountCode.account_direction(accounts(:bank_acct2).code)
    assert_equal -1, AccountCode.account_direction(accounts(:visa).code)
    assert_equal -1, AccountCode.account_direction(accounts(:salary).code)
    assert_equal 1, AccountCode.account_direction(accounts(:expense1).code)
  end

  test 'should get all accounts' do
    accts = @acct_code.accounts
    assert_equal accts.count, 4
    assert_includes accts, accounts(:bank_acct)
    assert_includes accts, accounts(:random_assets)
    assert_includes accts, accounts(:random_assets2)
    assert_includes accts, accounts(:bank_acct2)
  end

  test 'calculates current total correctly' do
    total_assets = 100 + 150 + 250 - 200 + 100  # 400
    assert_equal total_assets, @acct_code.total
  end

  test 'updates current total after new tx' do
    expense_amount = 300
    date = '2016-12-26'
    Tx.create(:date => date, :amount => expense_amount, :description => "grerffera", :dr => accounts(:expense1), :cr => accounts(:bank_acct))
    total_assets = 100 + 150 + 250 - 200 + 100 - expense_amount  # 100
    assert_equal total_assets, @acct_code.total
  end

  test 'calculates total at a given time correctly' do
    date = Time.new(2016, 12, 21)
    total_assets = 100 + 150 + 250 - 200  # 300
    assert_equal total_assets, @acct_code.total(date)
  end

  test 'calculates total changes between two times correctly' do
    date1 = Time.new(2015, 1, 11)
    date2 = Time.new(2016, 12, 25)
    assert_equal 0, @acct_code.total(date1, date2)

    date1 = Time.new(2015, 1, 1)
    date2 = Time.new(2015, 1, 4)
    change = 100 + 150 + 250  # 500
    assert_equal change, @acct_code.total(date1, date2)
  end

  test 'properly handles total being passed a nil value of date1' do
    assert_raise do
      @acct_code.total(nil)
    end
  end
end
