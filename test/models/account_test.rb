require 'test_helper'

class AccountTest < ActiveSupport::TestCase

  def setup
    @acct = accounts(:bank_acct)
    @salary = accounts(:salary)
  end

  # ==== Validation ====

  test 'should be valid' do
    assert @acct.valid?
  end

  test 'should not accept duplicate codes' do
    a = Account.new(:code => @acct.code, :name => "fjawkefj")
    assert_not a.valid?
  end

  test 'should not accept duplicate names' do
    a = Account.new(:code => 160, :name => @acct.name)
    assert_not a.valid?
  end

  test 'should not accept blank names' do
    @acct.name = "     "
    assert_not @acct.valid?
  end

  test 'should not accept blank codes' do
    @acct.code = nil
    assert_not @acct.valid?
  end

  test 'code should match a pattern' do
    @acct.code = "900"
    assert_not @acct.valid?
  end

  # ==== Balance and changes ====

  test 'should get current balance' do
    balance = 250  # based on fixture data
    assert_equal balance, @acct.balance
    @acct.code = "200"  # change to liability account
    assert_equal -balance, @acct.balance
  end

  test 'should update balance after new tx' do
    amount = 72
    assert_difference "@acct.balance", amount do
      Tx.create(:amount => amount, :date => Date.yesterday, :dr => @acct, :cr => @salary)
    end
    assert_difference "@acct.balance", -amount do
      Tx.create(:amount => amount, :date => Date.yesterday, :dr => @salary, :cr => @acct)
    end
  end

  test 'should get balance at given moment' do
    amount = 100  # fixture data
    assert_equal amount, @acct.balance(Time.new(2015, 1, 2))
  end

  test 'should correctly get changes over given periods' do
    assert_equal   0, @acct.change(Time.new(2015, 1, 1), Time.new(2015, 1, 1))
    assert_equal 100, @acct.change(Time.new(2015, 1, 1), Time.new(2015, 1, 2))
    assert_equal 250, @acct.change(Time.new(2015, 1, 1), Time.new(2015, 1, 3))
    assert_equal 500, @acct.change(Time.new(2015, 1, 1), Time.new(2015, 1, 4))
    assert_equal   0, @acct.change(Time.new(2015, 2, 1), Time.new(2015, 3, 1))
  end

  # ==== Misc ====

  test 'should get all matching account types' do
    types = @acct.account_types
    assert types.include?(account_codes(:assets))
    assert types.include?(account_codes(:current_assets))
  end
end
