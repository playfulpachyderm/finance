require 'test_helper'

class TxTest < ActiveSupport::TestCase

  def setup
    @tx = txes(:paycheque1)
  end

  test 'association works properly' do
    assert_equal accounts(:bank_acct), @tx.dr
    assert_equal accounts(:salary), @tx.cr

    assert accounts(:bank_acct).drs.include?(@tx)
  end

  # ==== Validation ====

  test 'should be valid' do
    assert @tx.valid?
  end

  test 'should require amount' do
    @tx.amount = nil
    assert_not @tx.valid?
  end

  test 'amount should be positive' do
    @tx.amount = 0
    assert_not @tx.valid?

    @tx.amount = -1.3
    assert_not @tx.valid?
  end

  test 'should have dr' do
    @tx.dr = nil
    assert_not @tx.valid?

    dr = Account.new
    @tx.dr = dr
    assert_not @tx.valid?
  end

  test 'should have cr' do
    @tx.cr = nil
    assert_not @tx.valid?

    cr = Account.new
    @tx.cr = cr
    assert_not @tx.valid?
  end

  test 'should have date' do
    @tx.date = nil
    assert_not @tx.valid?
  end
end
