require 'test_helper'

class FinancialStatementsHelperTest < ActionView::TestCase
  test 'date format (human readable)' do
    assert_equal "December 25, 2016", date_format_human(Time.new(2016, 12, 25))
    assert_equal "February 29, 12344", date_format_human(Time.new(12344, 2, 29))
    assert_equal "January 30, 2016", date_format_human(Time.new(2016, 1, 30))
  end

  test 'date format (database / param format)' do
    assert_equal "2016-01-02", date_format_db(Time.new(2016, 1, 2))
  end

  test 'parse date from param' do
    param = "2016-01-02"
    date = Time.new(2016, 1, 2)
    default = Time.new(2016, 3, 1)

    assert_equal date, parse_date(param)
    assert_equal date, parse_date(param, default)
    assert_equal default, parse_date(nil, default)
    assert_equal Time.now.beginning_of_day, parse_date(nil).beginning_of_day
  end

  test 'format balance' do
    assert_equal "--", format_balance(0)
    assert_equal "--", format_balance(-0)
    assert_equal "--", format_balance(-0.0)
    assert_equal "12.00", format_balance(12)
    assert_equal "12.00", format_balance(12.004)
    assert_equal "12.01", format_balance(12.008)
  end
end
