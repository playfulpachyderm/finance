require 'test_helper'

class FinancialStatementsControllerTest < ActionDispatch::IntegrationTest
  include FinancialStatementsHelper

  def _test_row(slug, balance)
    assert_select "tr.#{slug}" do
      assert_select "td.total-name"
      assert_select "td.total-balance", format_balance(balance)
    end
  end

  def _test_row_counts(row_count)
    assert_select ".account-name", :count => row_count
    assert_select ".account-balance", :count => row_count
  end

  def _test_block(names, dates = nil)
    names.each do |category|
      code = AccountCode.find_by(:name => category)
      slug = "total-#{code.name.parameterize}"
      balance = code.total(*dates)
      _test_row(slug, balance)
    end
  end

  # ==== Balance sheet ====

  test "should get today's balance sheet" do
    get balance_sheet_path
    assert_response :success
    assert_template "financial_statements/balance_sheet"

    row_count = AccountCode.find_by(:name => "ASSETS").accounts.count + AccountCode.find_by(:name => "LIABILITIES").accounts.count
    _test_row_counts(row_count)

    _test_block(["CURRENT ASSETS", "LONG TERM ASSETS", "ASSETS", "CURRENT LIABILITIES", "LONG TERM LIABILITIES", "LIABILITIES"])

    balance = AccountCode.find_by(:name => "ASSETS").total - AccountCode.find_by(:name => "LIABILITIES").total
    slug = "net-worth"
    _test_row(slug, balance)
  end

  test "should get an old balance sheet" do
    date = Time.new(2015, 1, 5)
    get balance_sheet_path, :params => { :date => date_format_db(date) }
    assert_response :success
    assert_template "financial_statements/balance_sheet"

    row_count = AccountCode.find_by(:name => "ASSETS").accounts.count + AccountCode.find_by(:name => "LIABILITIES").accounts.count
    _test_row_counts(row_count)

    _test_block(["CURRENT ASSETS", "LONG TERM ASSETS", "ASSETS", "CURRENT LIABILITIES", "LONG TERM LIABILITIES", "LIABILITIES"], [date])

    balance = AccountCode.find_by(:name => "ASSETS").total(date) - AccountCode.find_by(:name => "LIABILITIES").total(date)
    slug = "net-worth"
    _test_row(slug, balance)
  end

  # ==== Income statement ====

  test "should get all-time income statement" do
    get income_statement_path
    assert_template "financial_statements/income_statement"

    row_count = AccountCode.find_by(:name => "REVENUE").accounts.count + AccountCode.where("name like '%EXPENSES'").sum { |code| code.accounts.count }
    _test_row_counts(row_count)

    _test_block(["REVENUE", "LEGITIMATE EXPENSES", "OPTIONAL EXPENSES", "RETARDED EXPENSES", "MISC EXPENSES"])

    balance = AccountCode.find_by(:name => "REVENUE").total - AccountCode.where("name like '%EXPENSES'").sum { |code| code.total }
    slug = "net-income"
    _test_row(slug, balance)
  end

  test "should get income statement for give time period" do
    date1 = Time.new(2015, 1, 1)
    date2 = Time.new(2015, 2, 1)
    get income_statement_path, :params => { :date1 => date_format_db(date1), :date2 => date_format_db(date2) }
    assert_response :success
    assert_template "financial_statements/income_statement"

    row_count = AccountCode.find_by(:name => "REVENUE").accounts.count + AccountCode.where("name like '%EXPENSES'").sum { |code| code.accounts.count }
    _test_row_counts(row_count)

    _test_block(["REVENUE", "LEGITIMATE EXPENSES", "OPTIONAL EXPENSES", "RETARDED EXPENSES", "MISC EXPENSES"], [date1, date2])

    balance = AccountCode.find_by(:name => "REVENUE").total(date1, date2) - AccountCode.where("name like '%EXPENSES'").sum { |code| code.total(date1, date2) }
    slug = "net-income"
    _test_row(slug, balance)
  end
end
